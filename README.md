# Evaluations Projet 2023

* [I. Oral](#i.-oral)
  * [A. Critères d'évaluation](#a.-critères-d'évaluation)
  * [B. Forme attendue de la présentation](#b.-forme-attendue-de-la-présentation)
  * [C. Horaires de passage](#c.-horaires-de-passage)
* [II. Ecrit](#ii.-ecrit)

## I. Oral

L'oral est une présentation devant jury, prévu à la date du 15 Mai.

### A. Critères d'évaluation

Les **critères d'évaluation** sont les suivants, chacun sur 5 points : 

- **pertinence de la problématique**
  - est-ce que le sujet est d'actualité ?
  - est-ce que le sujet est dans l'air du temps ?
  - est-ce que votre façon de répondre à la problématique est pertinente ?
- **challenge recherché**
- **qualité de la présentation**
  - est-ce que c'était agréable de vous écouter/de vous regarder présenter ?
- **qualité de la démonstration**

### B. Forme attendue de la présentation

**Le flow de la présentation attendue, dans les grandes lignes :**

- présentation de la problématique (sans parler de technos spécifiques souvent)
- comment répondre à cette problématique
- votre réponse technique à la problématique, où vous prouvez que le besoin est comblé
- démonstration

**Le ton adopté :**

- professionnel, on est pas entre potes
- vous parlez à un jury technique
- pas de vulgarisation à outrance
- la juste vulgarisation nécessaire si votre sujet est touchy
- **l'idée est de parler au jury comme vous parleriez à un RSSI/responsable d'infra** que vous essayez de convaincre que votre solution est la meilleure pour répondre à la problématique énoncée

### C. Horaires de passage

[Voir le document dédié.](./horaires.md)

## II. Ecrit

**L'écrit est un dépôt Git.** Il contient : 

- tout le nécessaire pour remonter votre solution à l'identique
- documentation écrite, scripts d'install, fichiers de conf, suite d'opérations pour réaliser X, etc.

La notation sera adaptée en fonction de chaque sujet. Seront pris en compte :

- clarté du document
- est-ce que ça fonctionne ?Est-ce que la suite d'opérations est valide, cohérente et censée ?

**L'écrit est à remettre par MP Discord à Thibault train#9755 et Léo it4#9848 au plus tard le dimanche qui suit l'oral, soit le 21 Mai 2023 à 23h59 au plus tard.**

