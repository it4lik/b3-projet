| Heure    | Groupe                                 |
| ---      | ---                                    |
| 10h00    | Curmi, Joyet, Gélineau                 |
| 10h30    | Labaste, Schaffner, Quesnoy, Berguella |
| 11h00    | Roux, Peyrataux, Montagnier            |
| 12h00    | Bourin, Fallous, D'Autheville          |
| 12h30    | Champeau, Ben Ammar, Maury             |
| 14h15    | Perrin, Chasson, Mathieu               |
| 14h45    | Rogard, Gachassin, El Hor              |
| 15h15    | Roussat, Le Maguet, Flament            |

